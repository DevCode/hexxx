#ifndef UTIL_HEADER
#define UTIL_HEADER

typedef struct {
	void **data;
	int len;
	int index;
} dyn_array;

typedef struct {
	dyn_array *keys;
	dyn_array *data;
} keymap;

int is_number(char *str);
void print_hexdata(char *data, int rows, size_t len);

int is_equal(char *str, char *str2);

int get_element_size(char *type);
int get_element_type(char *type);

keymap *create_keymap(int initial_size);
void *get_value_by_key(keymap *map, char *key);
int get_index(keymap *map, char *key);
int add_pair(keymap *map, char *key, void *data, int data_len);
int resize_map(keymap *map, int new_len);
int remove_pair(keymap *map, char *key);
void free_keymap(keymap *map);

dyn_array *create_dyn_array(int initial_size);
int add_value(dyn_array *array, void *data, int data_len);
int remove_value(dyn_array *array, int index);
void* get_value(dyn_array *array, int index);
int resize_array(dyn_array *array, int new_len);
void free_dyn_array(dyn_array *array);

void delete_unprintable_sequences(char *str);
void remove_leading_space(char *str);

long tolongbasex(char *str);
long getbits(void *value, size_t element_size);
int gettype(char *str);
#endif
