#ifndef PROC_HEADER
#define PROC_HEADER

#define MAX_PATH_LEN 256
#define BUF_LEN 2048

typedef struct dirent DIRENTRY;
typedef FILE file;

#define MAX_VMAP_BOUNDS 128

typedef struct {
	off_t start;
	off_t end;
	size_t size;
	char permissions[64];
} vmap;

typedef struct {
	int pid;
	int vmap_len;
	int process_memory_fd;
	char *process_name;
	file *process_memory_maps;
	vmap *maps;
} process;

static int get_pid_by_name(char *process_name);
static void get_process_name(int pid, char *process_name, int len);
process *get_process_by_pid(int pid);
process *get_process_by_name(char *process_name);
void update_vmem_bounds(process *proc);
void print_vmap_bounds(process *proc);
void print_proc_status(process *proc);
void free_proc(process *proc);
#endif
