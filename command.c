#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "util.h"
#include "command.h"
#include "proc.h"
#include "mem.h"

char *error_msg[0x9] = {
  "Not enough arguments !",
  "Argument has wrong type !",
  "No target program set !",
  "The target program is already set !",
  "There's no such command !",
  "Not in address range !",
  "Exit program !",
  "There's no such program !"
};

char *start_msg = " \
For help enter ? or h \n \
#  # ### #  #  #  #  #  #\n \
#### ##   ##    ##    ##\n \
#  # ### #  #  #  #  #  #\n";

char *commands_help[NUMBER_OF_COMMANDS] = {
  "? <command> : \n\
  gives a more detailed description for <command>",
  "search [type] [value] | <address> <len> : \n\
  search for the [value] with the type : [type] \n\
  in the range of <address>-<address>+<len>",
  "read [type] [address] : \n\
  read the [type] @ [address]",
  "write [type] [value] [address] : \n\
  write [value] with the type : [type] @ [address]",
  "readstring [address] : \n\
  read string starting from [address]",
  "writestring [string] [address] : \n\
  write [string] @ [address] ",
  "strings <string> <address> <len> : \n\
  look for strings if <string> is given search for \n\
  that string in the range of\n\
  <address>-<address+len>",
  "searchrefs [value] <address> <len> : \n\
  look for references to [value] in the range \n\
  <address>-<address+len>",
  "writepointer [value] [address] : \n\
  write pointer [value] @ [address]",
  "printvmaps : \n\
  print address space of the target program",
  "dumpmemorysec [address] [len] : \n\
  dump memory starting from [address] to [len]",
  "setproc [pid/name] : \n\
  set the target program with its [pid] or its [name]",
  "quit : \n\
  exit the program",
};

char user_indic[MAX_STRING_LEN] = ">";

command cmd[16];

process *proc;

void start_cmd() {
  create_command(&cmd[0], 0, 1, "?", (int []) {T_STR}, (int []) {OPTIONAL}, PROC_DONT_CARE, help_command);
  create_command(&cmd[1], 2, 2, "search", (int []) {T_STR, T_DEF, T_INT, T_INT},
    (int []) {REQUIRED, REQUIRED, OPTIONAL, OPTIONAL}, PROC_REQUIRED, search_value_command);
  create_command(&cmd[2], 2, 0, "read", (int []) {T_STR, T_INT},
    (int []) {REQUIRED, REQUIRED}, PROC_REQUIRED, read_command);
  create_command(&cmd[3], 3, 0, "write", (int []) {T_STR, T_DEF, T_INT},
    (int []) {REQUIRED, REQUIRED, REQUIRED}, PROC_REQUIRED, write_command);
  create_command(&cmd[4], 1, 0, "readstring", (int []) {T_INT},
    (int []) {REQUIRED}, PROC_REQUIRED, read_string_command);
  create_command(&cmd[5], 2, 0, "writestring", (int []) {T_STR, T_INT},
    (int []) {REQUIRED, REQUIRED}, PROC_REQUIRED, write_string_command);
  create_command(&cmd[6], 0, 3, "strings", (int []) {T_STR, T_INT, T_INT},
    (int []) {OPTIONAL, OPTIONAL, OPTIONAL}, PROC_REQUIRED, strings_command);
  create_command(&cmd[7], 1, 2, "searchrefs", (int []) {T_INT, T_INT, T_INT},
    (int []) {REQUIRED, OPTIONAL, OPTIONAL}, PROC_REQUIRED, search_reference_command);
  create_command(&cmd[8], 2, 0, "writepointer", (int []) {T_STR, T_INT},
    (int []) {REQUIRED, REQUIRED}, PROC_REQUIRED, write_pointer_command);
  create_command(&cmd[9], 0, 0, "printvmaps", (int []) {}, (int []) {}, PROC_REQUIRED, print_vmaps_command);
  create_command(&cmd[10], 0, 0, "dumpmemorysec", (int []) {}, (int []) {}, PROC_REQUIRED, help_command);
  create_command(&cmd[11], 1, 0, "setproc", (int []){T_STR}, (int []){REQUIRED}, PROC_NOT_REQUIRED, set_proc_command);
  create_command(&cmd[12], 0, 0, "quit", (int []) {}, (int []) {}, PROC_DONT_CARE, quit_command);
}

int parse_line() {
  char line[MAX_COMMAND_LEN];
  char *ptr = NULL;
  int len = 0;
  char args[MAX_ARGUMENT_COUNT][MAX_COMMAND_LEN] = {'\0'};
  arguments args_struct;
  int command_index = 0;
  int args_count = 0;
  fgets(line, MAX_COMMAND_LEN, stdin);
  *strchr(line, '\n') = '\0';
  len = strlen(line);
  ptr = line;

  while(ptr-line < len) {
    while(isspace(*ptr) && *ptr != '\0') ptr++;
    if (*ptr ==  '\0') break;
    int spn = *ptr == '\"' ? strcspn(++ptr, "\"") : strcspn(ptr, " ");
    strncpy(args[args_count++], ptr, spn);
    ptr += spn+1;
  }

  command_index = get_command_index(args[0]);

  if (command_index == -1) return NO_SUCH_COMMAND;

  if ((cmd[command_index].proc_required == PROC_REQUIRED) && proc == NULL) {
    printf("[ERROR] %s \n", error_msg[NO_TARGET_PROGRAM-1]);
    return 1;
  } else if ((cmd[command_index].proc_required) == PROC_NOT_REQUIRED && proc != NULL) {
    printf("[ERROR] %s \n", error_msg[ALREADY_SET_TARGET_PROGRAM-1]);
    return 1;
  }

  if (cmd[command_index].num_args > (args_count-1)) return NOT_ENOUGH_ARGUMENTS;

  args_struct.command = args[0];
  args_struct.command_index = command_index;
  args_struct.num_args = 0;
  int max_len = cmd[command_index].num_args + cmd[command_index].opt_args;

  for (int i = 1; i < args_count && (i-1) < max_len; i++) {
    if (((cmd[command_index].optional >> ((i-1)*BW)) & BITMASK_VAL) == IGNORED) break;
    unsigned int type = ((cmd[command_index].types >> ((i-1)*BW)) & BITMASK_VAL);
    unsigned int arg_type = gettype(args[i]);
    if (type == T_DEF && (i-1 > 0)) {
      type = get_element_type(args[i-1]);
    }

    if (type != arg_type && type != T_STR) {
      for (int j = 1; i < i; j++) free(args_struct.arg[j]);
      printf("[ERROR] %s \n", error_msg[ARG_WRONG_TYPE-1]);
      return 1;
    }

    if (type == T_INT) {
      long *value = malloc(sizeof(long));
      *value = tolongbasex(args[i]);
      args_struct.arg[i-1] = (void *) value;
    } else if (type == T_FLOAT) {
      double *value = malloc(sizeof(long));
      *value = atof(args[i]);
      args_struct.arg[i-1] = (void *) value;
    } else {
      char *value = malloc(strlen(args[i])+1);
      strcpy(value, args[i]);
      args_struct.arg[i-1] = (void *) value;
    }
    args_struct.num_args = i;
  }
  int ret = cmd[command_index].command_handle(&args_struct);

  for (int i = 0; i < args_struct.num_args; i++) free(args_struct.arg[i]);

  if (ret != NOTHING && ret != QUIT_PROGRAM) {
    printf("[ERROR] %s \n", error_msg[ret-1]);
  }
  return ret != 7;
}

static int get_command_index(char *name) {
  if (name == NULL) return -1;
  for (int i = 0; i < NUMBER_OF_COMMANDS; i++) {
    if (!strcmp(name, cmd[i].name)) return i;
  }
  return -1;
}

static int print_vmaps_command(arguments *args) {
  print_vmap_bounds(proc);
  return NOTHING;
}

static int set_proc_command(arguments *args) {
  char *process_name = (char *) args->arg[0]; // name or pid of the process

  proc = get_process_by_name(process_name);
  if (proc == NULL) {
    proc = get_process_by_pid(atoi(process_name)); // pid ?
  }

  if (proc == NULL) {
    return NO_SUCH_PROGRAM;
  }

  printf("Target Program : %s[%d] \n", proc->process_name, proc->pid);
  char new_user_indic[MAX_STRING_LEN];
  sprintf(new_user_indic, "%s~%s", proc->process_name, user_indic);
  strcpy(user_indic, new_user_indic);
  return NOTHING;
}

static int strings_command(arguments *args) {
  char *string_to_match = (char *) args->arg[0];
  char check = (args->num_args > 0);
  off_t addr = args->num_args > 1 ? *((off_t *) args->arg[1]) : 0;
  size_t len = args->num_args > 2 ? *((size_t *) args->arg[2]) : 0;

  if (addr == 0 && len == 0) { // alles durchgehen
    for (int i = 0; i < proc->vmap_len; i++) {
      mem_snapshot *snapshot = create_snapshot(proc, proc->maps[i].start, proc->maps[i].size);
      search_result *results = search_for_strings(snapshot);

      if (results->offsets->index != 0)
        printf("=====================[VMAP(0x%lx)]=====================\n", i);
      if (!check) {
        print_search_results(proc, results, T_STR, 0);
      } else {
        print_matched_strings(proc, results, string_to_match);
      }
      free_search_results(results);
      free_snapshot(snapshot);
    }
  } else {
    if ((len != 0 && get_vmap_index_range(proc, addr, len) == -1) || get_vmap_index(proc, addr) == -1)
      return NOT_IN_ADDRESS_RANGE;

    mem_snapshot *snapshot = create_snapshot(proc, addr, len);
    search_result *results = search_for_strings(snapshot);

    if (!check) {
      print_search_results(proc, results, T_STR, 0);
    } else {
      print_matched_strings(proc, results, string_to_match);
    }
    free_search_results(results);
    free_snapshot(snapshot);
  }
  return NOTHING;
}

static int write_string_command(arguments *args) {
  char *string = (char *) args->arg[0];
  off_t addr = *((off_t *) args->arg[1]);

  if (get_vmap_index_range(proc, addr, strlen(string)+1) == -1)
    return NOT_IN_ADDRESS_RANGE;

  write_to_memory(proc, addr, string, strlen(string)+1);
  return NOTHING;
}

static int read_string_command(arguments *args) {
  char string[MAX_STRING_LEN];
  off_t addr = *((off_t *) args->arg[0]);

  if (get_vmap_index_range(proc, addr, 1) == -1)
    return NOT_IN_ADDRESS_RANGE;

  read_string_from_memory(proc, addr, string, MAX_STRING_LEN);
  printf("%s \n", string);
  return NOTHING;
}

static int write_pointer_command(arguments *args) {
  for (int i = args->num_args; i > 0; i--) {
    args->arg[i] = args->arg[i-1];
  }
  args->arg[0] = strdup("long");
  return write_command(args);
}

static int write_command(arguments *args) {
  int size = get_element_size((char *) args->arg[0]);
  int type = get_element_type((char *) args->arg[0]);
  long value = *((long *) args->arg[1]);
  off_t addr = *((off_t *) args->arg[2]);

  if (get_vmap_index_range(proc, addr, size) == -1)
    return NOT_IN_ADDRESS_RANGE;

  write_to_memory(proc, addr, &value, size);
  return NOTHING;
}

static int read_command(arguments *args) {
  int type = get_element_type((char *) args->arg[0]);
  int size = get_element_size((char *) args->arg[0]);
  off_t addr = *((off_t *) args->arg[1]);
  off_t value = 0;

  if (addr != 0 && get_vmap_index_range(proc, addr, size) == -1)
    return NOT_IN_ADDRESS_RANGE;

  read_from_memory(proc, addr, &value, size);
  if (type == T_INT)
    printf("[0x%lx] -> %ld \n", addr, value);
  else
    printf("[0x%lx] -> %f \n", addr, *((double*)((void *)value)));
  return NOTHING;
}

static int search_reference_command(arguments *args) { // insert type and search search_ref = search long val
  for (int i = args->num_args; i > 0; i--) {
    args->arg[i] = args->arg[i-1];
  }
  args->arg[0] = strdup("long");
  return search_value_command(args);
}

static int search_value_command(arguments *args) {
  int type = get_element_type((char *) args->arg[0]);
  int size = get_element_size((char *) args->arg[0]);
  long value = *((long *) args->arg[1]);
  off_t addr = 0;
  size_t len = 1;

  if (args->num_args > 2) addr = *((off_t *) args->arg[2]);
  if (args->num_args > 3) len = *((size_t *) args->arg[3]);

  if (addr != 0 && get_vmap_index(proc, addr) == -1)
    return NOT_IN_ADDRESS_RANGE;

  size_t max_len = proc->maps[get_vmap_index(proc, addr)].end - addr;
  len = (len > max_len || len == 0) ? max_len : len;

  if (addr != 0) {
    mem_snapshot *snapshot = create_snapshot(proc, addr, len);
    search_result *results = search_for_values(snapshot, &value, size, MEMORY_ALIGNED);

    print_search_results(proc, results, type, size);
    free_search_results(results);
    free_snapshot(snapshot);
  } else {
    for (int i = 0; i < proc->vmap_len; i++) {
      mem_snapshot *snapshot = create_snapshot(proc, proc->maps[i].start, proc->maps[i].size);
      search_result *results = search_for_values(snapshot, &value, size, MEMORY_ALIGNED);
      if (results->offsets->index != 0)
        printf("=====================[VMAP(0x%lx)]=====================\n", i);

      print_search_results(proc, results, type, size);
      free_search_results(results);
      free_snapshot(snapshot);
    }
  }
  return NOTHING;
}

static int quit_command(arguments *args) {
  return QUIT_PROGRAM;
}

static int help_command(arguments *args) {
  if (args->num_args == 0) {
    char line[MAX_COMMAND_LEN] = {'\0'};
    for (int i = 0; i < NUMBER_OF_COMMANDS; i++) { // read line so just the first line shows up
      int len = strcspn(commands_help[i], ":");
      len = len < MAX_COMMAND_LEN ? len : MAX_COMMAND_LEN;
      strncpy(line, commands_help[i], len);
      line[len] = '\0';
      printf("  %s \n", line);
    }
  } else {
    int index = get_command_index((char *) args->arg[0]);
    if (index == -1) return NO_SUCH_COMMAND;
    else printf(" %s\n", commands_help[index]);
  }
  return NOTHING;
}

static void create_command(command *dest, int num_args, int num_opt_args,
  char *cmd_name, int types[], int optional[], int proc_required, command_pointer command_handle) {
  dest->num_args = num_args;
  dest->opt_args = num_opt_args;
  dest->name = cmd_name;
  for (int i = 0; i < num_args+num_opt_args; i++)
    dest->types ^= ((types[i] & BITMASK_VAL) << (BW*i));

  for (int i = 0; i < num_args+num_opt_args; i++)
    dest->optional ^= ((optional[i] & BITMASK_VAL) << (BW*i));

  dest->proc_required = proc_required;
  dest->command_handle = command_handle;
}
