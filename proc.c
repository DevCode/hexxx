#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "proc.h"

static int get_pid_by_name(char *process_name) {
	DIR *proc = opendir("/proc");
	DIRENTRY *entry;

	char process_name_tmp[MAX_PATH_LEN];

	if (proc == NULL) return -1;

	while((entry = readdir(proc)) != NULL) {
		int pid = atoi(entry->d_name);
		if (pid == 0) continue;

		get_process_name(pid, &process_name_tmp[0], MAX_PATH_LEN);

		if (!strcmp(process_name_tmp, process_name)) {
			closedir(proc);
			return pid;
		}
	}
	closedir(proc);
	return -1;
}

static void get_process_name(int pid, char *process_name, int len) {
	char path[MAX_PATH_LEN];
	sprintf(path, "/proc/%d/comm", pid);
	file *proc_name = fopen(path, "r");
	if (proc_name != NULL) {
		fgets(process_name, MAX_PATH_LEN, proc_name);
		*strchr(process_name, '\n') = '\0';
		fclose(proc_name);
	}
}

process *get_process_by_pid(int pid) {
	if (pid == -1 || pid == 0)
		return NULL;

	char path_maps[MAX_PATH_LEN];
	char path_mem[MAX_PATH_LEN];
	char path_stat[MAX_PATH_LEN];

	sprintf(path_maps, "/proc/%d/maps", pid);
	sprintf(path_mem, "/proc/%d/mem", pid);
	sprintf(path_stat, "/proc/%d/status", pid);

	process *proc = malloc(sizeof(process));
	proc->pid = pid;
	proc->process_name = malloc(MAX_PATH_LEN);
	get_process_name(pid, proc->process_name, MAX_PATH_LEN);
	proc->process_memory_maps = fopen(path_maps, "r");
	proc->process_memory_fd = open(path_mem, O_RDWR);
	proc->maps = malloc(sizeof(vmap) * MAX_VMAP_BOUNDS);
	if (proc->process_memory_fd == -1 || proc->process_memory_maps == NULL) {
		free_proc(proc);
		return NULL;
	}
	update_vmem_bounds(proc);
	return proc;
}

process *get_process_by_name(char *process_name) {
	return  get_process_by_pid(get_pid_by_name(process_name));
}

void update_vmem_bounds(process *proc) {
	char line[BUF_LEN];
	int index = 0;
	fseek(proc->process_memory_maps, 0, SEEK_SET);
	char *ptr;
	while(fgets(line, BUF_LEN, proc->process_memory_maps) != NULL) {
		//*strchr(line, '\n') = '\0';
		char string[MAX_PATH_LEN] = {'\0'};
		sscanf(line, "%*s %*s %*s %*s %*s %s", string);
		if (strstr(string, proc->process_name) != NULL ||
		    strstr(string, "heap") != NULL ||
				strlen(string) == 0) {
			sscanf(line, "%lx-%lx %s", &proc->maps[index].start, &proc->maps[index].end, proc->maps[index].permissions);
				if (proc->maps[index].start != proc->maps[index].end) {
					proc->maps[index].size = proc->maps[index].end - proc->maps[index].start;
					index++;
				} else {
					proc->maps[index].start = 0;
					proc->maps[index].end = 0;
					proc->maps[index].permissions[0] = '\0';
				}
		}
	}
	proc->vmap_len = index;
}

void print_vmap_bounds(process *proc) {
	for (int i = 0; i < proc->vmap_len; i++)
		printf("[%d] 0x%lx - 0x%lx %lx %s \n", i, proc->maps[i].start, proc->maps[i].end, proc->maps[i].size, proc->maps[i].permissions);
}

void free_proc(process *proc) {
	if (proc == NULL)
		return;
	if (proc->process_name != NULL) free(proc->process_name);
	if (proc->process_memory_maps != NULL) fclose(proc->process_memory_maps);
	if (proc->process_memory_fd != -1) close(proc->process_memory_fd);
	if (proc->maps != NULL) free(proc->maps);
	free(proc);
}
