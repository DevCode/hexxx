#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <sys/types.h>
#include <unistd.h>

#include "util.h"
#include "proc.h"
#include "mem.h"
#include "command.h"

//TODO rewrite
void write_to_memory(process *proc, off_t addr, void *data, size_t len) {
	if (!is_in_vmaps(proc, addr, len)) return;

	off_t index = addr;
	size_t to_write = len;
	char *data_pos = (char *) data;
	while(to_write > 0) {
		size_t written = pwrite(proc->process_memory_fd, data_pos, to_write, index);
		index += written;
		to_write -= written;
		data_pos += written;
	}
}
//TODO rewrite
void read_from_memory(process *proc, off_t addr, void *data, size_t len) {
	if (!is_in_vmaps(proc, addr, len)) return;

	off_t index = addr;
	size_t to_read = len;
	char *data_pos = (char *) data;
	while(to_read > 0) {
		size_t read = pread(proc->process_memory_fd, data_pos, to_read, index);
		index += read;
		to_read -= read;
		data_pos += read;
	}
}

int is_in_vmaps(process *proc, const off_t range_addr, const size_t range_len) {
	return get_vmap_index_range(proc, range_addr, range_len) > -1 ? 1 : 0;
}

int get_vmap_index(process *proc, const off_t addr) {
	return get_vmap_index_range(proc, addr, 0);
}

int get_vmap_index_range(process *proc, const off_t range_addr, const size_t range_len) {
	off_t end = range_addr+range_len;
	for (int i = 0; i < proc->vmap_len; i++) {
		if (range_addr >= proc->maps[i].start && end <= proc->maps[i].end)	return i;
	}
	return -1;
}

void write_pointer_to_memory(process *proc, off_t ptr, off_t addr) {
	write_to_memory(proc, addr, &ptr, sizeof(off_t));
}

off_t read_pointer_from_memory(process *proc, off_t addr) {
	off_t ptr = NULL;
	read_from_memory(proc, addr, &ptr, sizeof(off_t));
	return ptr;
}

void read_string_from_memory(process *proc, off_t addr, char *str_buffer, size_t max_len) {
	memset(str_buffer, '\0', max_len);
	read_from_memory(proc, addr, str_buffer, max_len-1);
	str_buffer[max_len - 1] = '\0';
}

mem_snapshot *create_snapshot(process *proc, off_t addr, size_t len) {
	if (!is_in_vmaps(proc, addr, len)) return NULL;
	mem_snapshot *snapshot = malloc(sizeof(mem_snapshot));
	snapshot->addr = addr;
	snapshot->len = len;
	snapshot->data = malloc(len);
	read_from_memory(proc, snapshot->addr, snapshot->data, snapshot->len);
	return snapshot;
}

search_result *search_for_values(mem_snapshot *snapshot, void *value, size_t element_size, int flags) {
	off_t increment = element_size;

	unsigned long bitmask = 0;
	if (element_size < 8) bitmask = (1L << (element_size << 3)) - 1;
	else bitmask = ~bitmask;

	search_result *result = malloc(sizeof(search_result));
	result->offsets = create_dyn_array(INITIAL_ARRAY_SIZE);
	result->value = (*(long *) value) & bitmask;

	if (((snapshot->addr % element_size) != 0 && (flags & MEMORY_ALIGNED)) || (flags & MEMORY_UNALIGNED))
		increment = 1;

	for (off_t i = 0; i < snapshot->len; i+= increment) {
		long read_val = 0;
		off_t end = i+sizeof(long) < snapshot->len ? i+sizeof(long) : snapshot->len;
		for (off_t j = i; j < end; j++) // loop unrolling ?
			read_val ^= ((long) snapshot->data[j] << ((j-i) << 3)); // (j-i* 8)

		read_val = read_val & bitmask;
		if (read_val == result->value) { // found add one address
			off_t addr = i + snapshot->addr;
			int ret = add_value(result->offsets, &addr, sizeof(off_t));
			if (ret == -1) return result; // too many results
		}
	}
	return result;
}

search_result *search_for_strings(mem_snapshot *snapshot) {
	search_result *result = malloc(sizeof(search_result));
	result->offsets = create_dyn_array(INITIAL_ARRAY_SIZE);

	int index = 0;
	for (int i = 0; i < snapshot->len; i++) {
		if (isalnum(snapshot->data[i])) {
			off_t addr = i + snapshot->addr;
			int ret = add_value(result->offsets, &addr, sizeof(off_t));
			if (ret == -1) return result; // too many results
			index++;
			while(i < snapshot->len && snapshot->data[i] != '\0') i++;
		}
	}
	return result;
}

// check size because otherwise the results are wrong; store results->offsets->index
void print_search_results(process *proc, search_result *results, int type, int element_size) {
	if (type == T_INT) {
		for (int i = 0; i < results->offsets->index; i++) {
			printf(" | [0x%lx] -> %64d | \n", *((off_t *) get_value(results->offsets, i)), results->value);
		}
	} else if (type == T_FLOAT) {
		for (int i = 0; i < results->offsets->index; i++) {
			printf(" | [0x%lx] -> %64f | \n", *((off_t *) get_value(results->offsets, i)), *((double *) &results->value));
		}
	} else if (type == T_STR) {
		char string[MAX_STRING_LEN] = {'\0'};
		for (int i = 0; i < results->offsets->index; i++) {
			memset(string, '\0', MAX_STRING_LEN);
			read_string_from_memory(proc, *((off_t *) get_value(results->offsets, i)), string, MAX_STRING_LEN);
			delete_unprintable_sequences(string);
			printf("| [0x%lx] -> %64s | \n", *((off_t *) get_value(results->offsets, i)), string);
		}
	}
}

void print_matched_strings(process *proc, search_result *results, char *string_to_match) {
	char string[MAX_STRING_LEN] = {'\0'};
	for (int i = 0; i < results->offsets->index; i++) {
		memset(string, '\0', MAX_STRING_LEN);
		read_string_from_memory(proc, *((off_t *) get_value(results->offsets, i)), string, MAX_STRING_LEN);
		if (strstr(string, string_to_match) != NULL) {
			delete_unprintable_sequences(string);
			printf(" | [0x%lx] -> %64s | \n", *((off_t *) get_value(results->offsets, i)), string);
		}
	}
}

void free_snapshot(mem_snapshot *snapshot) {
	free(snapshot->data);
	free(snapshot);
}

void free_search_results(search_result *results) {
	free_dyn_array(results->offsets);
	free(results);
}
