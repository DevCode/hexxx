#ifndef COMMAND_HEADER
#define COMMAND_HEADER

#define PROC_NOT_REQUIRED 0b00
#define PROC_REQUIRED     0b01
#define PROC_DONT_CARE    0b10

#define T_INT   0b00
#define T_FLOAT 0b01
#define T_STR   0b10
#define T_DEF 	0b11

#define IGNORED  0b00
#define OPTIONAL 0b01
#define REQUIRED 0b10

#define BITMASK_VAL 0b11
#define BW 2

#define MAX_COMMAND_LEN 256
#define MAX_ARGUMENT_COUNT 16
#define NUMBER_OF_COMMANDS 13

#define NOTHING 0x0
#define NOT_ENOUGH_ARGUMENTS 0x1
#define ARG_WRONG_TYPE 0x2
#define NO_TARGET_PROGRAM 0x3
#define ALREADY_SET_TARGET_PROGRAM 0x4
#define NO_SUCH_COMMAND 0x5
#define NOT_IN_ADDRESS_RANGE 0x6
#define QUIT_PROGRAM 0x7
#define NO_SUCH_PROGRAM 0x8

typedef struct {
	void *arg[MAX_ARGUMENT_COUNT]; // arguments with the correct data_type as void * for easy casting later
	int types[MAX_ARGUMENT_COUNT]; // types of the arguments
	int num_args; // number of arguments
	int command_index; // index of the command in the command list
	char *command; // name of the command
} arguments;

typedef int (*command_pointer)(arguments *args);

typedef struct {
	int num_args; // min args
  int opt_args; // optional args
	char *name; // name of the command
	unsigned int types; // array of types (str, char, short, int, float, long, double)
	unsigned int optional; // which arguments are optional
	int proc_required; // is proc required to be initialized
	command_pointer command_handle;
} command;

void start_cmd();
int parse_line();
static int get_command_index(char *name);
static int help_command(arguments *args);
static int quit_command(arguments *args);
static int strings_command(arguments *args);
static int print_vmaps_command(arguments *args);
static int read_command(arguments *args);
static int read_string_command(arguments *args);
static int write_command(arguments *args);
static int write_pointer_command(arguments *args);
static int write_string_command(arguments *args);
static int search_value_command(arguments *args);
static int search_reference_command(arguments *args);
static int set_proc_command(arguments *args);
static void create_command(command *dest, int num_args, int num_opt_args,
	char *cmd_name, int types[], int optional[], int proc_required,
  command_pointer command_handle);
#endif
