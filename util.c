#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "command.h"
#include "mem.h"
#include "proc.h"
#include "util.h"

char *numbers = "0123456789";
char *numbers_float = "0123456789.";
char *hex_numbers = "0123456789ABCDEFabcdef";

dyn_array *create_dyn_array(int initial_size) {
	dyn_array *array = malloc(sizeof(dyn_array));
	array->index = 0;
	array->len = initial_size;
	array->data = malloc(initial_size * sizeof(void **));
	return array;
}

int add_value(dyn_array *array, void *data, int data_len) {
	if (array->index == array->len) {
		if (resize_array(array, array->len * 2) == -1)
			return -1;
	}
	array->data[array->index] = malloc(data_len);
	memcpy(array->data[array->index], data, data_len);
	array->index++;
	return 1;
}

int remove_value(dyn_array *array, int index) {
	if (array->index < index)
		return -1;

	free(array->data[index]);
	for (int i = index; i < array->index; i++) {
		array->data[i] = array->data[i+1];
	}
	array->index--;
	return 1;
}

// copies data so don't pass the len of the pointer
void* get_value(dyn_array *array, int index) {
	if (index < array->index && index >= 0) {
		return array->data[index];
	} else {
		return NULL;
	}
}

int resize_array(dyn_array *array, int new_len) {
	void **new_data = realloc(array->data, new_len * sizeof(void **));
	if (new_data != NULL) {
		array->len = new_len;
		array->data = new_data;
		return 1;
	}
	return -1;
}

void free_dyn_array(dyn_array *array) {
	for (int i = 0; i < array->index; i++) {
		free(array->data[i]);
	}
	free(array->data);
	free(array);
}

keymap *create_keymap(int initial_size) {
	keymap *map = malloc(sizeof(map));
	map->keys = create_dyn_array(initial_size);
	map->data = create_dyn_array(initial_size);
	return map;
}

int add_pair(keymap *map, char *key, void *data, int data_len) {
	int ret_key = add_value(map->keys, key, strlen(key)+1);
	int ret_val = add_value(map->data, data, data_len);
	if (ret_key && ret_val)
		return 1;
	return -1;
}

void *get_value_by_key(keymap *map, char *key) {
	int index = get_index(map, key);
	return index != -1 ? get_value(map->data, index) : NULL;
}

int get_index(keymap *map, char *key) {
	for (int i = 0; i < map->keys->index; i++) {
		if (is_equal((char *) get_value(map->keys, i), key)) return i;
	}
	return -1;
}

int remove_pair(keymap *map, char *key) {
	int index = get_index(map, key);
	if (index != -1) {
		remove_value(map->keys, index);
		remove_value(map->data, index);
	}
	return index;
}

void free_keymap(keymap *map) {
	free_dyn_array(map->keys);
	free_dyn_array(map->data);
	free(map);
}

int is_equal(char *str, char *str2) {
	return !strcmp(str, str2);
}

int get_element_size(char *type) {
	if (is_equal(type, "char"))
		return 1;
	else if (is_equal(type, "short"))
		return sizeof(short);
	else if (is_equal(type, "int") || is_equal(type, "float"))
		return sizeof(int);
	else if (is_equal(type, "long") || is_equal(type, "double"))
		return sizeof(long);
	return -1;
}

int get_element_type(char *type) {
	if (is_equal(type, "float") || is_equal(type, "double"))
		return T_FLOAT;
	else if (is_equal(type, "short") || is_equal(type, "int") || is_equal(type, "long") || is_equal(type, "char"))
		return T_INT;
	return -1;
}

void delete_unprintable_sequences(char *str) {
	int index = 0;
	while(str[index] != '\0') {
		if (iscntrl(str[index]) || !isgraph(str[index])) str[index] = '.';
		index++;
	}
}

long tolongbasex(char *str) {
	long value = 0;
	if (strstr(str, "0x") == str) sscanf(str, "%lx", &value);
	else sscanf(str, "%ld", &value);
	return value;
}

long getbits(void *value, size_t element_size) {
	unsigned long bitmask = 0;
	if (element_size < 8) bitmask = (1L << ( element_size << 3)) - 1;
	else bitmask = ~bitmask;

	return (*((long *) value)) & bitmask;
}

int gettype(char *str) {
	int str_len = strlen(str);
	if (strstr(str, "0x") == str) {
		if (strspn(str+2, hex_numbers) == str_len-2)
			return T_INT;
		else
			return T_STR;
	} else {
			if (strspn(str, numbers) == str_len) {
				return T_INT;
			} else { // check for float use variables
				char *dot = strchr(str, '.');
				int range_break = strspn(str, numbers_float);
				if ((dot == strrchr(str, '.')) && (dot != NULL) && (range_break == str_len))
					return T_FLOAT;
				else
					return T_STR;
			}
	}
}
