#include "proc.h"
#include "util.h"

#ifndef MEM_HEADER
#define MEM_HEADER

#define MAX_STRING_LEN 64
#define INITIAL_ARRAY_SIZE 32

typedef struct {
  off_t addr;
  size_t len;
  unsigned char *data;
} mem_snapshot;

typedef struct {
  dyn_array *offsets;
  long value;
} search_result;

#define MEMORY_UNALIGNED 	0b00001
#define MEMORY_ALIGNED 		0b00010

void write_to_memory(process *proc, off_t addr, void *data, size_t len);
void read_from_memory(process *proc, off_t addr, void *data, size_t len);
void read_string_from_memory(process *proc, off_t addr, char *str_buffer, size_t max_len);
void free_snapshot(mem_snapshot *snapshot);
void free_search_results(search_result *results);
int is_in_vmaps(process *proc, off_t const range_addr, const size_t range_len);
int get_vmap_index_range(process *proc, const off_t range_addr, const size_t range_len);
int get_vmap_index(process *proc, const off_t addr);
off_t read_pointer_from_memory(process *proc, off_t addr);
mem_snapshot *create_snapshot(process *proc, off_t addr, size_t len);
search_result *search_for_values(mem_snapshot *snapshot, void *value, size_t element_size, int flags);
search_result *search_for_strings(mem_snapshot *snapshot);
void print_search_results(process *proc, search_result *results, int type, int element_size);
void print_matched_strings(process *proc, search_result *results, char *string_to_match);
#endif
