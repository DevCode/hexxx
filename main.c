#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <curses.h>

#include "proc.h"
#include "command.h"

extern char user_indic[];
extern char *start_msg;
extern process *proc;

int main(int argc, char *argv[]) {
	if (geteuid() != 0) {
		printf("Check your privilige \n");
		return 0;
	}
	start_cmd();
	printf(start_msg);
	printf("%s", user_indic);
	while(parse_line()) {
		printf("%s", user_indic);
	}
	return 0;
}
